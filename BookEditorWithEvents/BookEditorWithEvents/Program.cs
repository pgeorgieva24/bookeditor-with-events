﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookEditorWithEvents
{
    class Program
    {
        static void Main(string[] args)
        {            
            Book first = new Book("c# in a nutshell", "Joseph Albahari & Ben Albahari", "summary", "content");
            BookEditor firstBookeditor = new BookEditor(first);
            Console.WriteLine(firstBookeditor.preview);
            firstBookeditor.TitleEdit += book_TitleChanged;
            firstBookeditor.AuthorEdit += book_AuthorChanged;
            firstBookeditor.SummaryEdit += book_SummaryChanged;
            firstBookeditor.ContentEdit += book_ContentChanged;
            
            firstBookeditor.EditTitle("new book title");
            Console.WriteLine();
            firstBookeditor.EditAuthor("new author - Joseph Albahari");
            Console.WriteLine();
            firstBookeditor.EditTitle("second new title");
            Console.WriteLine();
            firstBookeditor.EditSummary("summarysummarysummary");
            Console.WriteLine();

            Console.WriteLine(firstBookeditor.preview);

            Console.ReadLine();
        }

        static void book_TitleChanged(object sender, EditEventArgs e)
        {
            Console.WriteLine("The title was changed from " + e.OldString + " to " + e.NewString);
        }

        static void book_AuthorChanged(object sender, EditEventArgs e)
        {
            Console.WriteLine("The author was changed from " + e.OldString + " to " + e.NewString);
        }

        static void book_SummaryChanged(object sender, EditEventArgs e)
        {
            Console.WriteLine("The summary was changed from " + e.OldString + " to " + e.NewString);
        }

        static void book_ContentChanged(object sender, EditEventArgs e)
        {
            Console.WriteLine("The content was changed from " + e.OldString + " to " + e.NewString);
        }
    }


    
    
}
