﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookEditorWithEvents
{
    public class EditEventArgs : EventArgs
    {
        public readonly string OldString;
        public readonly string NewString;

        public EditEventArgs(string oldString, string newString)
        {
            OldString = oldString;
            NewString = newString;
        }
    }

    class BookEditor
    {
        Book book;
        public string preview;

        public event EventHandler<EditEventArgs> TitleEdit;
        public event EventHandler<EditEventArgs> AuthorEdit;
        public event EventHandler<EditEventArgs> SummaryEdit;
        public event EventHandler<EditEventArgs> ContentEdit;


        protected virtual void OnTitleEdit(EditEventArgs t)
        {
            TitleEdit?.Invoke(this, t);
        }

        protected virtual void OnAuthorEdit(EditEventArgs a)
        {
            AuthorEdit?.Invoke(this, a);
        }

        protected virtual void OnSummaryEdit(EditEventArgs s)
        {
            SummaryEdit?.Invoke(this, s);
        }
        protected virtual void OnContentEdit(EditEventArgs c)
        {
            ContentEdit?.Invoke(this, c);
        }

        public BookEditor(Book book)
        {
            this.book = book;
            preview = book.Title + " " + book.Author + " " + book.Summary + " " + book.Content;
        }

        public void EditTitle(string title)
        {
            string oldTitle = book.Title;
            book.Title = title;
            OnTitleEdit(new EditEventArgs(oldTitle, title));
            preview = book.Title + " " + book.Author + " " + book.Summary + " " + book.Content;
        }

        public void EditAuthor(string author)
        {
            string oldAuthor = book.Author;
            book.Author = author;
            OnAuthorEdit(new EditEventArgs(oldAuthor, author));
            preview = book.Title + " " + book.Author + " " + book.Summary + " " + book.Content;
        }

        public void EditSummary(string summary)
        {
            string oldSummary = book.Summary;
            book.Summary = summary;
            OnSummaryEdit(new EditEventArgs(oldSummary, summary));
            preview = book.Title + " " + book.Author + " " + book.Summary + " " + book.Content;
        }

        public void EditContent(string content)
        {
            string oldContent = book.Content;
            book.Content = content;
            OnContentEdit(new EditEventArgs(oldContent, content));
            preview = book.Title + " " + book.Author + " " + book.Summary + " " + book.Content;
        }
    }
}
