﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookEditorWithEvents
{
    class Book
    {
        string title;
        string author;
        string summary;
        string content;

        public string Title { get => title; set => title = value; }
        public string Author { get => author; set => author = value; }
        public string Summary { get => summary; set => summary = value; }
        public string Content { get => content; set => content = value; }

        public Book(string title, string author, string summary, string content)
        {
            Title = title;
            Author = author;
            Summary = summary;
            Content = content;
        }
    }
}
